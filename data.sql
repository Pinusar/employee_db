INSERT INTO users(name)
VALUES ('admin');

INSERT INTO languages(language)
VALUES ('English'),
       ('French'),
       ('Spanish');

INSERT INTO employee(name, birth_date, id_code, active, email, phone, address, created_at, created_by, last_modified_at, last_modified_by) VALUES (
'John Smith', '1974-03-01', '37403010000', true, 'johnsmith@company.com', '53912233', '3 Main street', now(), 1, now(), 1);

INSERT INTO employee_info(introduction, work_experience, education, employee_id, language_id, created_at, created_by, last_modified_at, last_modified_by) VALUES (
'Introduction EN', 'Experience EN', 'Education EN', 1, 1, now(), 1, now(), 1);
INSERT INTO employee_info(introduction, work_experience, education, employee_id, language_id, created_at, created_by, last_modified_at, last_modified_by) VALUES ('Introduction FR', 'Experience FR', 'Education FR', 1, 2, now(), 1, now(), 1);
INSERT INTO employee_info(introduction, work_experience, education, employee_id, language_id, created_at, created_by, last_modified_at, last_modified_by) VALUES ('Introduction ES', 'Experience ES', 'Education ES', 1, 3, now(), 1, now(), 1);
