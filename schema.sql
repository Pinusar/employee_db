DROP TABLE IF EXISTS employee_info;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS languages;

CREATE TABLE users
(
    id   INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200)
);

CREATE TABLE languages
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    language VARCHAR(50)
);

CREATE TABLE employee
(
    id               INT PRIMARY KEY AUTO_INCREMENT,
    name             VARCHAR(200),
    birth_date       DATETIME,
    id_code          VARCHAR(50),
    active           BOOLEAN,
    email            VARCHAR(100),
    phone            VARCHAR(50),
    address          TEXT,
    created_at       DATETIME,
    created_by       INT,
    last_modified_at DATETIME,
    last_modified_by INT,
    FOREIGN KEY (created_by) REFERENCES users (id),
    FOREIGN KEY (last_modified_by) REFERENCES users (id)
);

CREATE TABLE employee_info
(
    id               INT PRIMARY KEY AUTO_INCREMENT,
    introduction     TEXT,
    work_experience  TEXT,
    education        TEXT,
    employee_id      INT,
    language_id      INT,
    created_at       DATETIME,
    created_by       INT,
    last_modified_at DATETIME,
    last_modified_by INT,
    FOREIGN KEY (employee_id) REFERENCES employee (id),
    FOREIGN KEY (language_id) REFERENCES languages (id)
);
